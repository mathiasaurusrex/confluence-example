Example file structure mimicking the project requirements found in the Raymond James site:


https://horizontal.atlassian.net/wiki/display/RJADV/RJ+-+Advisor+Sites


Currently using [Jade](http://jade-lang.com/) for HTML preprocessing and [.scss](http://sass-lang.com/documentation/file.SCSS_FOR_SASS_USERS.html) for CSS preprocessing. Although the file structure is not dependent on these.


Jade Watch Command:

jade --watch src/jade/*.jade --out static/html


SCSS Watch Command:

sass --watch src/sass/main.scss:static/css/main.css